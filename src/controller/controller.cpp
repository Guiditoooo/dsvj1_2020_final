#include "controller.h"

namespace game {

	namespace controller {

		void start() {

			generalStart();
			gameloop::init();

			while (!WindowShouldClose()) {
				gameloop::update();
				gameloop::draw();
			}

			gameloop::deinit();

		}

		void generalStart() {

			InitWindow(screenWidth, screenHeight, "Tello Guido Final");
			SetTargetFPS(frameRate);

		}

	}

}