#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "gameloop/gameloop.h"

namespace game {

	namespace controller {

		static int screenWidth  = 800;
		static int screenHeight = 800;
		static int frameRate = 60;

		void start();
		void generalStart();
	
	}

}

#endif

