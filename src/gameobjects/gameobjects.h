#ifndef GAMEOBJECTS_H
#define GAMEOBJECTS_H
#include "raylib.h"
#include "controller/controller.h"

namespace game {

	namespace gameobjects {

		namespace player {

			namespace hb {

				const float WIDTH = controller::screenWidth / 12;
				const float HEIGHT = controller::screenHeight / 12;
				const float X = 50;
				const float Y = 50;

			}

			const Color color = RED;

		}

		namespace tile {

			const int QTY_COLOR = 3;
			const Color colorTiles[QTY_COLOR] = { DARKGRAY, RAYWHITE, SKYBLUE };
			const int MAT_DIM = 8;

			namespace hb {

				const float WIDTH = controller::screenWidth/10;
				const float HEIGHT = controller::screenHeight/10;

			}

		}

		struct Int2 {
			int x;
			int y;
		};
		
		struct Player {
			Rectangle hitbox = { 0,0,0,0 };
			Color color = BLACK;
			Int2 pos = { 0,0 };
		};

		extern Player p1;

		enum class TileType {
			WALL,
			FLOOR,
			PAINTED,
			NONE
		};

		struct Tiles {
			Rectangle hitbox = { 0,0,0,0 };
			TileType type = TileType::NONE;
			Color color = BLACK;
		};

		extern Tiles map[tile::MAT_DIM][tile::MAT_DIM];

	}

}

#endif

