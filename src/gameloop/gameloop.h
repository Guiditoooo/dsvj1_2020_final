#ifndef GAMELOOP_H
#define GAMELOOP_H
#include "raylib.h"

namespace game {

	namespace gameloop {
	
		void init();
		void update();
		void draw();
		void deinit();
		void levelReset();

		enum class Movement {
			UP,
			LEFT,
			DOWN,
			RIGHT,
			NONE
		};
		
		enum class Definition {
			PLAYING,
			WIN,
			LOSE
		};

	}

}

#endif

