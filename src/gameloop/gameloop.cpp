#include "gameloop.h"
#include "raylib.h"
#include "controller/controller.h"
#include "gameobjects/gameobjects.h"
#include <iostream>
#include <string>


namespace game {

	namespace gameloop {

		const float VELOCITY = 340;
		const int MAX_MOVES_ALLOWED = 23;
		
		gameobjects::Int2 startingPos = { 6,6 };

		Movement toMove[MAX_MOVES_ALLOWED]; // max 22
		Definition resolution;

		float frames = 0;

		bool pausing;
		int timesMoved;
		int timer;

		std::string presionedKey;
		bool msgShowed;
		bool canDraw;

		void init() {

			timesMoved = 0;

			levelReset();

		}		

		void update() {

			if (IsKeyPressed(KEY_P)) {
				pausing = !pausing;
			}

			if (IsKeyPressed(KEY_R)) {
				levelReset();
				timesMoved = 0;
				std::cout << "\n Reiniciaste\n";
			}

			if (resolution == Definition::PLAYING) {
				
				if (!pausing) {

					frames += GetFrameTime();
					if (frames >= 0.5f) {
						frames = 0;
						timer--;
					}

					char PressedCharacter = GetKeyPressed();
					std::string auxL;
					switch (PressedCharacter) {
					case 'a':
					case 'A':
						toMove[timesMoved] = Movement::LEFT;
						timesMoved++;
						std::cout << "\nMe movi izq\n";
						auxL = "L ";
						break;
					case 'w':
					case 'W':
						toMove[timesMoved] = Movement::UP;
						timesMoved++;
						std::cout << "\nMe movi arr\n";
						auxL = "U ";
						break;
					case 'd':
					case 'D':
						toMove[timesMoved] = Movement::RIGHT;
						timesMoved++;
						std::cout << "\nMe movi der\n";
						auxL = "R ";
						break;
					case 's':
					case 'S':
						toMove[timesMoved] = Movement::DOWN;
						timesMoved++;
						std::cout << "\nMe movi abajo\n";
						auxL = "D ";
						break;
					default:
						if (IsKeyPressed(KEY_LEFT)) {
							toMove[timesMoved] = Movement::LEFT;
							auxL = "L ";
							timesMoved++;
						}
						if (IsKeyPressed(KEY_UP)) {
							toMove[timesMoved] = Movement::UP;
							auxL = "U ";
							timesMoved++;
						}
						if (IsKeyPressed(KEY_RIGHT)) {
							toMove[timesMoved] = Movement::RIGHT;
							auxL = "R ";
							timesMoved++;
						}
						if (IsKeyPressed(KEY_DOWN)) {
							toMove[timesMoved] = Movement::DOWN;
							auxL = "D ";
							timesMoved++;
						}
						break;
					}

					presionedKey += auxL;


					if (IsKeyPressed(KEY_ENTER)) {
						canDraw = true;
						std::cout << "\nApretaste enter\n";
						if (timesMoved != MAX_MOVES_ALLOWED - 2) {
							resolution = Definition::LOSE;
						}
						else
						{
							using namespace gameobjects;

							for (short moves = 0; moves < timesMoved && resolution == Definition::PLAYING; moves++) {

								switch (toMove[moves]) {

								case game::gameloop::Movement::UP:
									p1.hitbox.y -= tile::hb::HEIGHT;
									p1.pos.y--;
									break;
								case game::gameloop::Movement::LEFT:
									p1.hitbox.x -= tile::hb::WIDTH;
									p1.pos.x--;
									break;
								case game::gameloop::Movement::DOWN:
									p1.hitbox.y += tile::hb::HEIGHT;
									p1.pos.y++;
									break;
								case game::gameloop::Movement::RIGHT:
									p1.hitbox.x += tile::hb::HEIGHT;
									p1.pos.x++;
									break;
								default:
									break;
								}

								if (CheckCollisionRecs(p1.hitbox, map[p1.pos.x][p1.pos.y].hitbox)) {
									if (map[p1.pos.x][p1.pos.y].type != TileType::WALL) {
										map[p1.pos.x][p1.pos.y].type = TileType::PAINTED;
									}
									else {
										resolution = Definition::LOSE;
									}
								}

							}

							for (short x = 0; x < tile::MAT_DIM; x++) {
								for (short y = 0; y < tile::MAT_DIM; y++) {
									if (map[x][y].type == TileType::FLOOR) {
										resolution = Definition::LOSE;
									}
									else {
										resolution = Definition::WIN;
									}
								}
							}

						}

					}

				}

			}

			else if (resolution == Definition::LOSE) {
				
				if (!msgShowed) {
					std::cout << "\n Perdiste!\n";
					msgShowed = true;
				}
			}
			else {
				if (!msgShowed) {
					std::cout << "\n Ganaste!\n";
					msgShowed = true;
				}
			}
					

		}

		void draw() {
		
			BeginDrawing();
			Rectangle winRec = { controller::screenWidth / 2 - 300 / 2 ,controller::screenHeight / 2 - 250 / 2 ,300,250 };
			if (resolution==Definition::PLAYING) {
				ClearBackground(RAYWHITE);

				using namespace gameobjects;

				for (short x = 0; x < tile::MAT_DIM; x++) {
					for (short y = 0; y < tile::MAT_DIM; y++) {
						DrawRectangleRec(map[x][y].hitbox, tile::colorTiles[static_cast<int>(map[x][y].type)]);
						DrawRectangleLinesEx(map[x][y].hitbox, 1, BLACK);
					}
				}
				if (canDraw) {

					DrawText(&presionedKey[0], controller::screenWidth / 20, controller::screenHeight * 28 / 30, 20, BLACK);
					
				}

				DrawRectangleRec(gameobjects::p1.hitbox, gameobjects::p1.color);

				std::string timerS = std::to_string(timer);

				DrawText(&timerS[0], controller::screenWidth *19 / 20, controller::screenHeight * 3 / 30, 20, BLACK);

			}
			else if(resolution == Definition::WIN) {
				ClearBackground(BLACK);
				DrawRectangleRec(winRec, BROWN);
				DrawText("YOU WIN", winRec.x + winRec.width/2 - MeasureText("YOU WIN", 50) / 2, winRec.y + winRec.height/2 - 50 / 2, 50, BLUE);
			}
			else if (resolution == Definition::LOSE) {
				ClearBackground(BLACK);
				DrawRectangleRec(winRec, BROWN);
				DrawText("YOU LOSE", winRec.x + winRec.width / 2 - MeasureText("YOU LOSE", 50) / 2, winRec.y + winRec.height / 2 - 50 / 2, 50, BLUE);
			}

			EndDrawing();

		}

		void deinit() {

			CloseWindow();

		}

		void levelReset() {

			msgShowed = false;
			pausing = false;
			resolution = Definition::PLAYING;
			canDraw = false;
			timer = 50;
			frames = 0;

			presionedKey = "";

			for (short i = 0; i < MAX_MOVES_ALLOWED; i++)
			{
				toMove[i] = Movement::NONE;
			}

			using namespace gameobjects;

			for (short x = 0; x < tile::MAT_DIM; x++) {
				for (short y = 0; y < tile::MAT_DIM; y++) {
					map[x][y].hitbox.width = tile::hb::WIDTH;
					map[x][y].hitbox.height = tile::hb::HEIGHT;
					map[x][y].hitbox.x = tile::hb::WIDTH + tile::hb::WIDTH * x;
					map[x][y].hitbox.y = tile::hb::HEIGHT + tile::hb::HEIGHT * y;
					map[x][y].type = TileType::WALL;
				}
			}

			for (short x = 0; x < tile::MAT_DIM; x++) {
				for (short y = 0; y < tile::MAT_DIM; y++) {
					bool paintFloor = false;
					if (y % 2 == 1 && x > 0 && x < tile::MAT_DIM - 1 && y < tile::MAT_DIM - 1) {
						map[x][y].type = TileType::FLOOR;
					}
				}
			}

			map[6][2].type = TileType::FLOOR;
			map[1][4].type = TileType::FLOOR;
			map[1][2].type = TileType::FLOOR;
			map[6][6].type = TileType::PAINTED;

			for (short x = 0; x < tile::MAT_DIM; x++) {
				for (short y = 0; y < tile::MAT_DIM; y++) {
					map[x][y].color = tile::colorTiles[static_cast<int>(map[x][y].type)];
				}
			}

			p1.hitbox = { map[6][6].hitbox.x + map[6][6].hitbox.width / 2 - player::hb::WIDTH / 2, map[6][6].hitbox.y + map[6][6].hitbox.height / 2 - player::hb::HEIGHT / 2,player::hb::WIDTH, player::hb::HEIGHT };
			p1.color = player::color;
			p1.pos = startingPos;

		}

	}

}